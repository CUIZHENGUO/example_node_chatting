var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/',function(req,resp){
 // resp.send('<h1>Hello World</h1>');
  resp.sendFile(__dirname + '/index.html');
});
app.get('/message',function(req,resp){
  resp.send('this is message');
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnect');
  });
  socket.on('chat message', function(msg){
   console.log('message:'+msg);
   io.emit('chat message', msg);
 });

});

http.listen(3000,function(){
  console.log('listening on *:3000');
});

